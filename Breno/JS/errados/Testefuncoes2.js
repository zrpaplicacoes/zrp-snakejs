$(document).keydown(function(event){
		switch(event.which){ //funciona como o else if so que para multiplos casos
		case 37:
		case 65:
			window.keyPressed = "left";
		break;
		case 87:
		case 38:
			window.keyPressed = "up";
		break;
		case 39:
		case 68:
			window.keyPressed = "right";
		break;
		case 83:
		case 40:
			window.keyPressed = "down";
		break;

	}
});

window.VERTICAL_SIZE = 10 //tamanho da minha grid do jogo
window.HORIZONTAL_SIZE = 10 //tamanho da minha grid do jogo

function ondeEstou(numeroDiv){
	var linha = Math.ceil(numeroDiv/window.VERTICAL_SIZE), //estou arredondando a divizão por float
	    coluna = numeroDiv%window.HORIZONTAL_SIZE;
	return [linha , coluna];
};

window.numerosDiv=[]
window.keyPressed;
window.terminar = false;
window.score = 0;

window.direcao="right";
var coin;
var pessoa;
var parede = [];
var level;
var points = [];
var zombies = [direcao, 1, 1];
var speed= 200;

function init(){
	criarZombies();
	criarPessoa();
	criarMoeda();
	criarParede();
	criarChao();
	level = 3; //3 para level 1, 6 para level 2, 9 para outros
	$('#pontuacao').text(score);

	 setTimeout(function(){gameupdate()}, speed);
}

// function caracterizarZombie() {
// 	var point;
// 	var numeros = [];
// 	$(".zombie-homem").each(function() {
// 		numeros.push($(this).index()+1);
// 	});
// 	for (i = 0; i<=numeros.lenght-1; i++){
// 		point = [direcao, ondeEstou(numeros[i])[0], ondeEstou(numeros[i])[1]]
// 		points.push(point);
// 		}
// 	delete window.zombies;
// 	window.zombies = points;
// 	return zombies;
// }
function gameupdate() {
	verDirecao()
	qualDiv()
	removeZombie()
	andar()
	addZombie()
	colisaoParede()
	colisaoMoeda()
	colisaoPessoa()
	colisaoChao()
	colisaoZombie()
	mudarLevel()
	criarChao()
 setTimeout(function(){gameupdate()}, speed);




}
function criarZombies() {
	$(".pixel:nth-child(1)").addClass("zombie-homem")
};

function criarPessoa() {
	pessoa = parseInt(Math.random()*100);
	$(".pixel:nth-child(" + pessoa + ")").addClass("pessoa")
};

function criarMoeda() {
	coin = parseInt(Math.random()*100);
	$(".pixel:nth-child(" + coin + ")").addClass("moeda")
};

function criarParede() {
	for (var i = 0; i <= level-1; i++){
		var paredeBate = parseInt(Math.random()*100);
		if (paredeBate == 1) {
			parede.push(parseInt(Math.random()*100));
		};
		if(paredeBate !=1){
			parede.push(paredeBate);
		};
		//pop retiraria o ultimo elemento
		$(".pixel:nth-child(" + parede[i] + ")").addClass("parede");
	};
};

function criarChao() {
	for (var h = 1; h<=100; h++){
		$(".pixel:nth-child("+ h +")").addClass("chao-1")
	}
	$('div.zombie-homem.chao-1').removeClass("chao-1")
	$('div.parede.chao-1').removeClass("chao-1")
	$('div.moeda.chao-1').removeClass("chao-1")
	$('div.pessoa.chao-1').removeClass("chao-1")
};

function mudarLevel() {
	if (score == 10) {
		level = 3; //ta criando mais 3 paredes
		criarParede();
	};
	if (score == 20) {
		level = 3; //ta fazendo mais tres paredes alem das que ja tem
		criarParede();
	};
};

function verDirecao() {
	window.direcao = window.keyPressed;
}

function andar() {
	for (var i=zombies.length-1; i>=0; i--){
		if (zombies[i][0] == "up"){
			if (zombies.length > 1){
				zombies[i][0] = zombies[i-1][0];
			};
			if(zombies.length == 1) {
				zombies[i][0] = direcao;
			};
			if (zombies[i][2] == 0){
				zombies[i][2] = 9;
			};
			if (zombies[i][2] != 0) {
				zombies[i][2] = zombies[i][2] + 1;
			};
		};
		if (zombies[i][0] == "down"){
			if (zombies.length > 1){
				zombies[i][0] = zombies[i-1][0];
			};
			if (zombies.length == 1) {
				zombies[i][0] = direcao;
			};
			if (zombies[i][2] == 9){
				zombies[i][2] = 0;
			};
			if (zombies[i][2] != 9) {
				zombies[i][2] = zombies[i][2] - 1;
			};
		};
		if (zombies[i][0] == "left") {
			if (zombies.length > 1){
				zombies[i][0] = zombies[i-1][0];
			};
			if(zombies.length == 1) {
				zombies[i][0] = direcao;
			};
			if (zombies[i][1] == 0){
				zombies[i][1] = 9;
			};
			if (zombies[i][1] != 0) {
				zombies[i][1] = zombies[i][1] - 1;
			};
		};
		if (zombies[i][0] == "right") {
			if (zombies.length > 1){
				zombies[i][0] = zombies[i-1][0];
			};
			if(zombies.length == 1) {
				zombies[i][0] = direcao;
			};
			if (zombies[i][1] == 9){
				zombies[i][1] = 0;
			};
			if (zombies[i][1] != 9) {
				zombies[i][1] = zombies[i][1] + 1;
			};
		};
		if (zombies[i][0] == "parado") {
			zombies[i][0] = zombies[i-1][0];
		};
	};
};

function removeZombie() {
	var oneDimensionSelector =qualDiv();
	for (var i=0; i<=zombies.length-1; i++) {
		$(".pixel:nth-child(" + oneDimensionSelector + ")").removeClass("zombie-homem");
	}
};

function addZombie() {
	for (var i=zombies.length-1; i>=0; i--) {
		var oneDimensionSelector = zombies[i][2]*10 + zombies[i][1];
		$(".pixel:nth-child(" + oneDimensionSelector + ")").addClass("zombie-homem");
	}
};

function qualDiv() {
	var numeros = [];
	$(".zombie-homem").each(function() {
		numeros.push($(this).index()+1);
	});
	return numeros;
};

function colisaoParede() {
	if ($('div.zombie-homem.parede')){
		window.terminar = true;
		alert('You lost !');    
    return;
	};
};
function colisaoZombie() {
	if ($('div.zombie-homem.zombie-homem')){
		window.terminar = true;
		alert('You lost !');    
    return;
	};
};
function colisaoMoeda() {
	if ($('div.zombie-homem.moeda')){
		window.score += 1;
		$('div.zombie-homem.moeda').removeClass("moeda")
		criarMoeda();
	};
};

function colisaoPessoa() {
	if ($('div.zombie-homem.pessoa')) {
		window.score += 1;
		$('div.zombie-homem.pessoa').removeClass("pessoa");
		zombies.push(["parado", zombies[0][1], zombies[0][2]]);
		criarPessoa();
	};
};

function colisaoChao() {
	if ($('div.zombie-homem.chao-1')) {
		$('div.zombie-homem.chao-1').removeClass("chao-1")
	};
};

// function engineRun() {
// 	engineLoop();
// };

// function renderRun() {
// 	renderLoop();
// };
// window.tempoAtualiza = 14 //em milissegundos

// function renderLoop(){
// 	window.setTimeRender = setTimeout(function(){
// 		renderRun(window.keyPressed);
// 		removeZombie();
// 		addZombie();
// 		criarChao();
// 		renderLoop();
// 	}, tempoAtualiza)
// };
// window.tempoAtualiza = 100 //em milissegundos

// function engineLoop(){
// 	window.setTimeEngine = setTimeout(function(){
// 		engineRun(window.keyPressed);
// 		//caracterizarZombie();
// 		andar();
// 		colisaoChao();
// 		colisaoPessoa();
// 		colisaoMoeda();
// 		colisaoParede();
// 		mudarLevel();
// 		engineLoop();
// 	}, tempoAtualiza)
// };
