window.tempoAtualiza = 340 //em milissegundos

function engineLoop(){
	window.setTimeEngine = setTimeout(function(){
		if (window.terminar == false) {
			verDirecao();
			qualDiv();
			andarLeft();
			andarRight();
			andarUp();
			andarDown();
			andarParado();
			mudarDirUp();
			mudarDirDown();
			mudarDirLeft();
			mudarDirRight();
			colisaoPessoa();
			colisaoMoeda();
			colisaoParede();
			paredePessoa();
			paredeMoeda();
			moedaPessoa();
			mudarLevel();
			$('#pontuacao').text(score);
			engineLoop();
		}
	}, tempoAtualiza)
};
