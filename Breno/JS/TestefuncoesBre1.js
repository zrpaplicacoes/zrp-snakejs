$(document).keydown(function(event){
		switch(event.which){ //funciona como o else if so que para multiplos casos
		case 37 :
		case 65 :
			window.keyPressed = "left";
			event.preventDefault();
		break;
		case 87 :
		case 38 :
			window.keyPressed = "up";
			event.preventDefault();
		break;
		case 39 :
		case 68 :
			window.keyPressed = "right";
			event.preventDefault();
		break;
		case 83 :
		case 40 :
			window.keyPressed = "down";
			event.preventDefault();
		break;

	}
});

window.VERTICAL_SIZE = 10 //tamanho da minha grid do jogo
window.HORIZONTAL_SIZE = 10 //tamanho da minha grid do jogo

function ondeEstou(numeroDiv){
	var linha = Math.ceil(numeroDiv/window.VERTICAL_SIZE), //estou arredondando a divizão por float
	    coluna = numeroDiv%window.HORIZONTAL_SIZE;
	return [linha , coluna];
};

window.numerosDiv=[]
window.keyPressed;
window.terminar = false;
window.iniciar = true;
window.score = 0;
window.direcao="right";

var coin;
var pessoa;
var parede = [];
var level;
var points = [];
var speed = 300;
var nivel2 = true;
var nivel3 = true;
var zombies = [[direcao, 1, 1]];


function init(){
	level = 3; 
	criarZombies();
	criarPessoa();
	criarMoeda();
	criarParede();
	criarChao();
	$('#pontuacao').text(score);
	renderLoop();
	engineLoop();
																	//setTimeout(function(){gameupdate()}, speed);
}

// function caracterizarZombie() {
// 	var point;
// 	var numeros = [];
// 	$(".zombie-homem").each(function() {
// 		numeros.push($(this).index()+1);
// 	});
// 	for (i = 0; i<=numeros.lenght-1; i++){
// 		point = [direcao, ondeEstou(numeros[i])[0], ondeEstou(numeros[i])[1]]
// 		points.push(point);
// 		}
// 	delete window.zombies;
// 	window.zombies = points;
// 	return zombies;
// }
																		// function gameupdate() {
																		// 	if (window.terminar == false) {
																		// 		verDirecao();
																		// 		qualDiv();
																		// 		removeZombie();
																		// 		removeCabeca();
																		// 		andar();
																		// 		addZombie();
																		// 		addCabeca();
																		// 		colisaoParede();
																		// 		colisaoMoeda();
																		// 		colisaoPessoa();
																		// 		//colisaoChao();
																		// 		colisaoZombie();
																		// 		mudarLevel();
																		// 		criarChao();
																		// 		$('#pontuacao').text(score);
																		// 		gameupdate();
																		// 	}
																		// }

function criarZombies() {
	$(".pixel:nth-child(1)").addClass("zombie-head");
};

function criarPessoa() {
	pessoa = parseInt(Math.random()*100);
	$(".pixel:nth-child(" + pessoa + ")").addClass("pessoa")
};

function criarMoeda() {
	coin = parseInt(Math.random()*100);
	$(".pixel:nth-child(" + coin + ")").addClass("moeda")
};

function criarParede() {
	parede = [];
	$('pixel').removeClass("parede");
	for (var i = 0; i <= level-1; i++){
		var paredeBate = parseInt(Math.random()*100);
		if (paredeBate == 1) {
			parede.push(parseInt(Math.random()*100));
		}else{
			parede.push(paredeBate);
		};
		//pop retiraria o ultimo elemento
		$(".pixel:nth-child(" + parede[i] + ")").addClass("parede");
	};
};

function criarChao() {
	for (var h = 1; h<=100; h++){
		$(".pixel:nth-child("+ h +")").addClass("chao-1");
	};
	$('div.zombie-homem.chao-1').removeClass("chao-1");
	$('div.parede.chao-1').removeClass("chao-1");
	$('div.moeda.chao-1').removeClass("chao-1");
	$('div.pessoa.chao-1').removeClass("chao-1");
};

function mudarLevel() {
	if (score == 10 && nivel2 == true) {
		level = 2; //ta criando mais 3 paredes
		criarParede();
		nivel2 = false;
	};
	if (score == 15 && nivel3==true) {
		level = 3; //ta fazendo mais tres paredes alem das que ja tem
		criarParede();
		nivel3 = false;
	};
};

function verDirecao() {
	if (window.keyPressed != undefined){
		window.direcao = window.keyPressed;
	};
};

function andarUp() {
	for (var i=zombies.length-1; i>=0; i--){
		if (zombies[i][0] == "up") {
			if (zombies[i][1] == 0) {
				zombies[i][1] = 9;
			}else{
				zombies[i][1] = zombies[i][1] - 1;
			};
		};
	};
};

function mudarDirUp() {
	for (var i=zombies.length-1; i>=0; i--){
		if (zombies[i][0] == "up"){
			if (zombies.length > 1 && i>0){
				zombies[i][0] = zombies[i-1][0];
			};
			if (zombies.length>1 && i == 0) {
				if (direcao != "down") {
					zombies[0][0] = direcao;
				}				
			};
			if(zombies.length == 1) {
				zombies[i][0] = direcao;
			};
		}
	}
}

function andarDown() {
	for (var i=zombies.length-1; i>=0; i--){
		if (zombies[i][0] == "down"){
			if (zombies[i][1] == 9){
				zombies[i][1] = 0;
			}else{
				zombies[i][1] = zombies[i][1] + 1;
			};
		};
	};
};

function mudarDirDown() {
	for (var i=zombies.length-1; i>=0; i--){
		if (zombies[i][0] == "down"){
			if (zombies.length > 1 && i>0){
				zombies[i][0] = zombies[i-1][0];
			};
			if (zombies.length>1 && i == 0) {
				if (direcao != "up") {
					zombies[0][0] = direcao;
				}				
			};
			if (zombies.length == 1) {
				zombies[i][0] = direcao;
			};
		}
	}
}

function andarLeft() {
	for (var i=zombies.length-1; i>=0; i--){
		if (zombies[i][0] == "left") {
			if (zombies[i][2] == 1){
				zombies[i][2] = 10;
			}else {
				zombies[i][2] = zombies[i][2] - 1;
			};
		};
	};
};

function mudarDirLeft() {
	for (var i=zombies.length-1; i>=0; i--){
		if (zombies[i][0] == "left") {
			if (zombies.length > 1 && i>0){
				zombies[i][0] = zombies[i-1][0];
			};
			if (zombies.length>1 && i == 0) {
				if (direcao != "right") {
					zombies[0][0] = direcao;
				}				
			};			if(zombies.length == 1) {
				zombies[i][0] = direcao;
			};
		}
	}
}

function andarRight() {
	for (var i=zombies.length-1; i>=0; i--){
		if (zombies[i][0] == "right") {
			if (zombies[i][2] == 10){
				zombies[i][2] = 1;
			}else {
				zombies[i][2] = zombies[i][2] + 1;
			};
		};
	};
};

function mudarDirRight() {
	for (var i=zombies.length-1; i>=0; i--){
		if (zombies[i][0] == "right") {
			if (zombies.length > 1 && i>0){
				zombies[i][0] = zombies[i-1][0];
			};
			if (zombies.length>1 && i == 0) {
				if (direcao != "left") {
					zombies[0][0] = direcao;
				}				
			};
			if(zombies.length == 1) {
				zombies[i][0] = direcao;
			};
		}
	}
}

function andarParado() {
	for (var i=zombies.length-1; i>=0; i--){	
		if (zombies[i][0] == "parado") {
			zombies[i][0] = zombies[i-1][0];
		};
	};
};
	

function removeZombie() {
	$(".pixel").removeClass("zombie-homem")
};

function addZombie() {
	for (var i = 1; i<=zombies.length - 1; i++) {
		var oneDimensionSelector = (zombies[i][1])*10 + zombies[i][2];
		console.log(oneDimensionSelector)
		$(".pixel:nth-child(" + oneDimensionSelector + ")").addClass("zombie-homem");
	}
};

function addCabeca() {
	var oneDimensionSelector = (zombies[0][1])*10 + zombies[0][2];
	$(".pixel:nth-child(" + oneDimensionSelector + ")").addClass("zombie-head");
};

function removeCabeca() {
	$(".pixel").removeClass("zombie-head")
};

function qualDiv() {
	var numeros = [];
	$(".zombie-homem").each(function() {
		numeros.push($(this).index()+1);
	});
	return numeros;
};

function limparTela(){
	$('.pixel').removeClass("zombie-head")
	$('.pixel').removeClass("zombie-homem")
	$('.pixel').removeClass("moeda")
	$('.pixel').removeClass("pessoa")
	$('.pixel').removeClass("parede")
}

function colisaoParede() {
	if ($('.pixel.zombie-homem.parede').length !=0 || $('.pixel.zombie-head.parede').length !=0){
		window.terminar = true;
		alert('You lost parede!');
	};
};
function colisaoZombie() {
	var colizombie = []
	colizombie.push($('.pixel.zombie-head.zombie-homem'));
	console.log(colizombie[0])
	if ($('.pixel.zombie-head.zombie-homem').length != 0){
		//debugger;
		window.terminar = true;
		alert('You lost zombi!');    
    delete colizombie;
	};
};
function colisaoMoeda() {
	if ($('.pixel.zombie-homem.moeda').length !=0 || $('.pixel.zombie-head.moeda').length !=0){
		window.score += 1;
		$(".pixel").removeClass("moeda")
		criarMoeda();
	};
};

function colisaoPessoa() {
	if ($('.pixel.zombie-homem.pessoa').length !=0 || $('.pixel.zombie-head.pessoa').length !=0) {
		window.score += 1;
		$(".pixel").removeClass("pessoa");
		zombies.push(["parado", zombies[zombies.length-1][1], zombies[zombies.length-1][2]]);
		criarPessoa();
	};
};

function paredePessoa() {
	if ($('.pixel.parede.pessoa').length !=0) {
		$(".pixel").removeClass("pessoa");
		criarPessoa();
	}
}

function paredeMoeda() {
	if ($('.pixel.parede.moeda').length !=0) {
		$(".pixel").removeClass("moeda");
		criarMoeda();
	}
}

function moedaPessoa() {
	if ($('.pixel.moeda.pessoa').length !=0) {
		$(".pixel").removeClass("moeda");
		criarMoeda();
	}
}

function pausar() {
	window.terminar = true;
};

function retomar() {
	window.terminar = false
}

function reiniciar() {
	limparTela();
	zombies = [["right", 1, 1]];
	score = 0;
	level = 3;
	criarPessoa();
	criarMoeda();
	criarParede();
	criarZombies();
	criarChao();
	window.jaAndei = false;
	window.terminar = false
	$('#pontuacao').text(score);
	renderLoop();
	engineLoop();
}
// function colisaoChao() {
// 	if ($('.pixel.zombie-homem.chao-1').length !=0) {
// 		$('div.zombie-homem.chao-1').removeClass("chao-1")
// 	};
// };

// function engineRun() {
// 	engineLoop();
// };

// function renderRun() {
// 	renderLoop();
// };
// window.tempoAtualiza = 14 //em milissegundos

// function renderLoop(){
// 	window.setTimeRender = setTimeout(function(){
// 		renderRun(window.keyPressed);
// 		removeZombie();
// 		addZombie();
// 		criarChao();
// 		renderLoop();
// 	}, tempoAtualiza)
// };
// window.tempoAtualiza = 100 //em milissegundos

// function engineLoop(){
// 	window.setTimeEngine = setTimeout(function(){
// 		engineRun(window.keyPressed);
// 		//caracterizarZombie();
// 		andar();
// 		colisaoChao();
// 		colisaoPessoa();
// 		colisaoMoeda();
// 		colisaoParede();
// 		mudarLevel();
// 		engineLoop();
// 	}, tempoAtualiza)
// };
