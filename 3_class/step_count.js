$(document).ready(function() {
  var currentStep = 0;

  $(".js-step").hide();
  $(".js-step-0").fadeIn(450);

  $(".js-click-step").on("click", function(event) {
    event.preventDefault();

    currentStep++;
    $(".js-step-" + currentStep).fadeIn(450);
  });
});