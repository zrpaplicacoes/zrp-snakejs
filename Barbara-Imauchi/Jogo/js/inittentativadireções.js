// $(document).ready(function() {
// 	var setGamebox = function() {
// 		var gamebox = $('.game-box');
// 		for(var i = 0; i < 400; i ++) {
// 			gamebox.append("<div class='pixel'></div>")
// 		}
// 		gamebox.append("<div class='clear'></div>");
// // 	};

$(document).ready(function(){
var speed='';
 var dir=4;
 var snake=["10_10","10_9","10_8"];
 var food=""; 
 var qtdcomida=[];
 var level=[];
 var parede='';
 var total='';
 var totallevel='';
 var dirAtual= 1;
 var directions= [];
 var jogoComecou = false;
 window.init = myinit;

initscreen();
 function myinit(){
    dir=4;
    snake=["10_10","10_9","10_8"];
    food="";
    qtdcomida=[];
    level=[];
    parede='';
    totallevel='';
    total='';

    initscreen();
    $('.score').text('Seus Pontos!');
      level.push('up');
      levelup();
      startGame();
      setTimeout(function(){gameupdate()}, speed);

  
 }
function initscreen() {

    $('.game-box').html("");
    for (var r=0;r<20;r++){
      for (var c=0;c<20;c++){
        $('.game-box').append('<div class=pixel id=q_'+r+'_'+c+'></div>');
        }
    }
    $('.game-box').append("<div class='limpar'></div>");

      $('#q_10_8').addClass('snake-body');
      $('#q_10_9').addClass('snake-body');
      $('#q_10_10').addClass('snake-body');
      $('.ongame').addClass('inicio-fim');
      newfood();
}
 
 function newfood(){
    var r1 = Math.floor(Math.random() * 19);
    var c1 = Math.floor(Math.random() * 19);
    $('#q_'+r1+'_'+c1).addClass('fruit');
    food=''+r1+'_'+c1;
    if ($('#q_'+food).hasClass('snake-body')){
      $('#q_'+food).removeClass('fruit');
      newfood()
    }
    if ($('#q_'+food).hasClass('parede')){
      $('#q_'+food).removeClass('fruit');
      newfood()
    }
 }

/*function newparedelevel() {
    for (var i=0; i=totallevel-1; i++) {
      newparede();
    }
}*/

 function newparede(){
  for (var i=0; i<3; i++) {
    var r2 = Math.floor(Math.random() * 19);
    var c2 = Math.floor(Math.random() * 19);
    $('#q_'+r2+'_'+c2).addClass('parede');
    wall=''+r2+'_'+c2;
    if ($('#q_'+wall).hasClass('snake-body')){
      $('#q_'+wall).removeClass('parede');
      newparede()
  }}}
  
 function levelup(){
    totallevel=Math.floor(total/1000 + 1);
    multtotal= (total%1000);
    if (multtotal==0){
    $('.levelscore').text(totallevel);
    newparede();
    speedup();
  }
 }

 function speedup(){
  speed=((1/totallevel)*200);
  }

function pontos(){
   qtdcomida.push('fruta');
      total=qtdcomida.length*100;
      $('.score').text(total);
}

function directionControl() {
  var hh=snake[0];
  var rc=hh.split("_");
  var r=parseInt(rc[0]);
  var c=parseInt(rc[1]);
  directions.push(dir);
  console.info(dirAtual);
  switch(dir){
    case 1: r=r+1; break; // baixo
    case 2: c=c-1; break; // esquerda
    case 3: r=r-1; break; // cima
    case 4: c=c+1; break;  // direita
  }

  dirAtual = dir;
  return { r: r, c: c};
}

function endGame() {
    $('.ongame').addClass('inicio-fim');
  $(".start.buttonend").removeClass('buttonend');
}

function startGame() {
  if (jogoComecou=true) {
  $('.ongame.inicio-fim').removeClass('inicio-fim');
  $(".start").addClass('buttonend');
  }
}

function gameupdate(){
  var tail=snake.pop(); 
  $('#q_'+tail).removeClass('snake-body');
  
  var row_column = directionControl();

  r = row_column["r"];
  c = row_column["c"];

  var head= r+"_"+c;
  if (head==food){
      snake.push(tail);
      $('#q_'+tail).addClass('snake-body');
      $('#q_'+food).removeClass('fruit');
      pontos();
      levelup();
      newfood();
  }

  snake.unshift(head);
  $('#q_'+head).hasClass('snake-body'); 
  //condition to exist the Game !
  if (c<0 || r<0 || c>19 || r>19 || $('#q_'+head).hasClass('snake-body')){
    console.info(directions);    
    jogoComecou = false;
    endGame();
    return;
  }
  if ($('#q_'+head).hasClass('parede')){
    jogoComecou = false;
    endGame();    
    return;
  }  
  $('#q_'+head).addClass('snake-body');       
  setTimeout(function(){gameupdate()}, speed);
 }

$(".start").click(function(event) {
  if (jogoComecou) {
    event.preventDefault();
    console.info("não pode clicar em começou mais");
  } else {
    window.init();
    jogoComecou = true;
    startGame();
  };
 });

$(document).keydown(function(e){
    e.preventDefault();
    if (e.keyCode == 37 && dirAtual!=4) { 
       dir=2;
    }
    else if (e.keyCode == 38 && dirAtual!=1) { 
       dir=3;
    }
    else if (e.keyCode == 39 && dirAtual!=2) { 
       dir=4;
    }
    else if (e.keyCode == 40 && dirAtual!=3) { 
       dir=1;
    }
});



});
 