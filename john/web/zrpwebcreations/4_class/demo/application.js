var addCar = function (carNumber, callbackFn) {
  // Recupera um carro através de uma função
  // passada como callback
  var car = callbackFn(carNumber);
  this.cars.push(car);
}

var getCarByNumber = function(carNumber) {
  // Retorna um carro baseado no número
  // passado do carro. 0 por exemplo => Porsche
  var cars = [
    { name: "Porsche", image: "porsche.jpg" },
    { name: "BMW"    , image: "bmw.jpg"     },
    { name: "Volvo"  , image: "volvo.jpg"   }
  ];
  return cars[carNumber];
}

var addCarToPageAndStore = function(carStore) {
  // Escolhe um número inteiro entre 0 e 2 (3 carros)
  var carNumber = parseInt((Math.random() * 3));

  // Aqui adiciona o novo carro na loja,
  // usando uma função que retorna um carro
  carStore.addCar(carNumber, getCarByNumber);

  // Aqui recupera esse carro e gera um html
  // para adicioná-lo na página
  var car = getCarByNumber(carNumber);
  var div = "<div class='car'>" +
              "<img src='" + car.image + "'/>" +
              "<p>" +  car.name + "</p>" +
            "</div>";
  $(".js-cars-list").append(div);

  // Escreve no console que um carro
  // foi adicionado, bem como o nome
  // do carro que foi adicionado!
  console.info("added car " + car.name + " to page!");
}

$(document).ready(function() {
  var carStore = {
    cars: [],
    addCar: addCar
  };

  // Binda o evento de clique no body
  // a adição de um carro na loja e na
  // página, usando as funções anteriores
  $("body").on('click', '.js-add-car', function(event) {
    event.preventDefault();
    addCarToPageAndStore(carStore);
  });
});