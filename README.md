# SnakeJS

SnakeJS is a JavaScript based game developed to teach basic front-end development and game design concepts, also providing a rich way of exercise creativity and basic code skills, such as clean and maintainable code, team work and open source (community) development.

# Version
0.0.1

# Tech

We'd like to thanks this particular projects for helping us teaching great front-end tools to our students!
* [jQuery] - duh
* [Bootstrap] - duh 2x

# Current Class Snippets

Some cheats that may be useful in second class.
#### Command Line Interfaces (CLI)
* CLI Unix Based Systems: http://learntocodewith.me/command-line/unix-command-cheat-sheet/
* CLI Unix Based Systems: https://ubuntudanmark.dk/filer/fwunixref.pdf
* CLI Windows: http://simplyadvanced.net/blog/cheat-sheet-for-windows-command-prompt/
#### Command Line Interfaces (Git)
* Best Git Book Ever!!!: https://git-scm.com/doc
* Git Cheats: http://www.git-tower.com/blog/git-cheat-sheet/
* Git Cheats: https://training.github.com/kit/downloads/github-git-cheat-sheet.pdf
* GitHub: https://github.com/
* GitLab: https://gitlab.com
* BitBucket: https://bitbucket.org/
----
# License
This game was produced by ZRP, a web company based on Sao Paulo.  Our page is available at [here.][zrp]
MIT

*ZRP, EASING IDEAS*
   
   [zrp]: <http://zrp.com.br>



